#include <stdio.h>
#include <stdlib.h>
#define MAX 5

int visited[MAX][MAX]; // visited(>1) | not visited(0)
int x[MAX];            // Toa do hien tai cua x: 0 -> 7
int y[MAX];            // Toa do hien tai cua y: 0 -> 7
int count = 0;

int dx[8] = {2, 1, -1, -2, -2, -1, 1, 2};
int dy[8] = {1, 2, 2, 1, -1, -2, -2, -1};

int isValid(int x, int y)
{
  if (x >= 0 && x < MAX && y >= 0 && y < MAX && visited[x][y] == 0)
    return 1;
  else
    return 0;
}

void init()
{
  for (int i = 0; i < MAX; i++)
    for (int j = 0; j < MAX; j++)
      visited[i][j] = 0;
}

void printSolution()
{
  count++;
  printf("Solution %d\n ", count);
  for (int i = 0; i < MAX * MAX; i++)
    printf("(%d,%d)", x[i], y[i]);

  printf("END\n");
}

void TRY(int k)
{
  if (k == MAX * MAX)
  {
    printSolution(); // Congrats, all squares are visited!
    return;
  }

  for (int t = 0; t < 8; t++)
  {
    x[k] = x[k - 1] + dx[t];
    y[k] = y[k - 1] + dy[t];

    if (isValid(x[k], y[k]))
    {
      // printf("%d.Moving %d:%d -> %d:%d\n", k, x[k - 1], y[k - 1], x[k], y[k]);
      visited[x[k]][y[k]] == 1;
      TRY(k + 1);
      visited[x[k]][y[k]] == 0;
    }
  }
}

int main()
{
  printf("Start knight's tour: \n");
  init();

  x[0] = 0;
  y[0] = 0;
  visited[0][0]++;
  TRY(1);
  printf("The number of solution is: %d\n", count);
}
