#include <stdio.h>
#define MAX 21

int z[MAX];
int A[MAX][MAX];
int w[MAX][MAX];    //weight
char color[MAX];    //W - WHITE: not visited
                    //G - GRAY: visited
//int connect[MAX][MAX]; //connect[u][v] = 1 if (u,v) is an edge
int min = 99*MAX;

int n, m, k;

void input_from_file(char filename[]){
    FILE *f;
    f = fopen(filename, "r");
    if (f == NULL){
        printf("\nCannot open file %s\n", filename);
        return;
    }
    fscanf(f, "%d %d %d\n", &n, &m, &k);
    int i;
    int v, u, d;
    for (i=1; i<=m; i++){
        fscanf(f, "%d %d %d\n", &v, &u, &d);
        A[v][z[v]++] = u;
        A[u][z[u]++] = v;
        //connect[u][v] = 1;
        //connect[v][u] = 1;
        w[v][u] = d;
        w[u][v] = d;
    }
    fclose(f);
}

void input_from_STDIN(){
    scanf("%d %d %d\n", &n, &m, &k);
    int i;
    int v, u, d;
    for (i=1; i<=m; i++){
        scanf("%d %d %d\n", &v, &u, &d);
        A[v][z[v]++] = u;
        A[u][z[u]++] = v;
        //connect[u][v] = 1;
        //connect[v][u] = 1;
        w[v][u] = d;
        w[u][v] = d;
    }
}

void initcolor(){
    int v;
    for (v=1; v<=n; v++) color[v] = 'W';
}

void DFS (int u, int k, int start, int totalweight){
    color[u] = 'G';
    if (k == 0){
        color[u] = 'W';
        //if (connect[u][start]){
        if (w[u][start]){
            totalweight += w[u][start];
            if (totalweight < min) min = totalweight;
        }
        return;
    }
    int i;
    for (i=0; i<z[u]; i++){
        int v = A[u][i];

        if (color[v] == 'W' && totalweight+w[u][v] < min){
            //totalweight += w[u][v];
            DFS(v, k-1, start, totalweight+w[u][v]);
        }
    }
    color[u] = 'W';
}

void solve(){
    input_from_file("KCYCLE.txt");
    //input_from_STDIN();
    initcolor();
    int i;
    for (i=1; i<=n-(k-1); i++){
        DFS(i, k-1, i, 0);
        color[i] = 'G';
    }
    printf("%d\n", min);
}


int main(){
    solve();

    return 0;
}
