#include <stdio.h>
#define MAX 4001

int n, m;
int z[MAX];
int A[MAX][MAX];
char color[MAX];    //W - WHITE: not visited
                    //G - GRAY: visited
int rela[MAX][MAX]; //rela[u][v] = 1 if u and v have friend relationship
int count;  //the number of 3-entities

void input_from_file(char* filename){
    FILE *f;
    f = fopen(filename, "r");
    if (f == NULL){
        printf("\nCannot open file %s\n", filename);
        return;
    }
    fscanf(f, "%d %d", &n, &m);
    int i;
    int v, u;
    for (i=1; i<=m; i++){
        fscanf(f, "%d %d\n", &v, &u);
        A[v][z[v]++] = u;
        A[u][z[u]++] = v;
        rela[u][v] = 1;
        rela[v][u] = 1;
    }
    fclose(f);
}

void input_from_STDIN(){
    scanf("%d %d", &n, &m);
    int i;
    int v, u;
    for (i=1; i<=m; i++){
        scanf("%d %d\n", &v, &u);
        A[v][z[v]++] = u;
        A[u][z[u]++] = v;
        rela[u][v] = 1;
        rela[v][u] = 1;
    }
}

void initcolor(){
    int v;
    for (v=1; v<=n; v++) color[v] = 'W';
}

void DFS (int u, int k, int start){
    color[u] = 'G';
    if (k == 0){
        color[u] = 'W';
        if (rela[u][start]) count++;
        return;
    }
    int i;
    for (i=0; i<z[u]; i++){
        int v = A[u][i];
        if (color[v] == 'W') DFS(v, k-1, start);
    }
    color[u] = 'W';
}

void solve(){
    input_from_file("ENTITY.txt");
    //input_from_STDIN();
    initcolor();
    int k = 3;
    int i;
    for (i=1; i<=n-(k-1); i++){
        DFS(i, k-1, i);
        color[i] = 'G';
    }
    printf("%d\n", count/2);
}

int main(){
    solve();

    return 0;
}
