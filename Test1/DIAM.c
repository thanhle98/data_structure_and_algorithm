#include <stdio.h>
#define MAX 201

int n, m;
int z[MAX];
int A[MAX][MAX];
char color[MAX]; //W - WHITE: not visited
                 //G - GRAY: visited
int diam;        //diameter of graph

void input_from_file(char *filename)
{
    FILE *f;
    f = fopen(filename, "r");
    if (f == NULL)
    {
        printf("\nCannot open file %s\n", filename);
        return;
    }
    fscanf(f, "%d %d", &n, &m);
    int i, v, u;
    for (i = 1; i <= m; i++)
    {
        fscanf(f, "%d %d\n", &v, &u);
        A[v][z[v]++] = u;
        A[u][z[u]++] = v;
    }
    fclose(f);
}

void input_from_STDIN()
{
    scanf("%d %d", &n, &m);
    int i, v, u;
    for (i = 1; i <= m; i++)
    {
        scanf("%d %d\n", &v, &u);
        A[v][z[v]++] = u;
        A[u][z[u]++] = v;
    }
}

typedef int elementtype;
typedef struct
{
    elementtype elements[MAX];
    int front;
    int rear;
} Queue;

void makeNullQueue(Queue *Q)
{
    Q->front = -1;
    Q->rear = -1;
}

int isEmptyQueue(Queue Q)
{
    return Q.front == -1;
}

int isFullQueue(Queue Q)
{
    return (Q.rear - Q.front + 1) == MAX;
}

void EnQueue(elementtype x, Queue *Q)
{
    if (!isFullQueue(*Q))
    {
        if (isEmptyQueue(*Q))
            Q->front = 0;
        Q->rear = Q->rear + 1;
        Q->elements[Q->rear] = x;
    }
    else
        printf("\nQueue is full!\n");
}

elementtype DeQueue(Queue *Q)
{
    elementtype item;
    if (!isEmptyQueue(*Q))
    {
        item = Q->elements[Q->front];
        Q->front = Q->front + 1;
        if (Q->front > Q->rear)
            makeNullQueue(Q);
    }
    else
    {
        //printf("\nQueue is empty!\n");
        item = -1;
    }
    return item;
}

int dist[MAX];

void init()
{
    for (int v = 1; v <= n; v++)
    {
        color[v] = 'W';
        dist[v] = 0;
    }
}

void BFS(int u)
{
    Queue Q;
    makeNullQueue(&Q);

    color[u] = 'G';
    EnQueue(u, &Q);
    dist[u] = 0;
    while (!isEmptyQueue(Q))
    {
        int v = DeQueue(&Q);
        for (int i = 0; i < z[v]; i++)
        {
            int tmp = A[v][i];
            //if (tmp == b) check = 1;
            if (color[tmp] == 'W')
            {
                color[tmp] = 'G';
                dist[tmp] = dist[v] + 1;
                EnQueue(tmp, &Q);
            }
        }
    }
}

void solve()
{
    input_from_file("DIAM.txt");
    //input_from_STDIN();
    int i, j;
    for (i = 1; i <= n; i++)
    {
        init();
        BFS(i);
        for (j = i + 1; j <= n; j++)
        {
            if (dist[j] > diam)
                diam = dist[j];
        }
    }
    printf("%d\n", diam);
}

int main()
{
    solve();
    return 0;
}
