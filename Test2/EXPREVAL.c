#include <stdio.h>
#define MAX 100000
#define PP 1000000007

int n;
int a[MAX];
int b[MAX]; //0 is "-", 1 is "+", 2 is "*"
int res;

void input_from_file(char *filename)
{
    FILE *f;
    f = fopen(filename, "r");
    if (f == NULL)
    {
        printf("\nCannot open file %s\n", filename);
        return;
    }
    fscanf(f, "%d", &n);
    int i;
    for (i = 0; i < n; i++)
        fscanf(f, "%d", &a[i]);
    for (i = 1; i < n; i++)
        fscanf(f, "%d", &b[i]);
}

void input_from_STDIN()
{
    scanf("%d", &n);
    int i;
    for (i = 0; i < n; i++)
        scanf("%d", &a[i]);
    for (i = 1; i < n; i++)
        scanf("%d", &b[i]);
}

int add(int a, int b) // a + b with overflow checked
{
    a = a % PP;
    b = b % PP;
    if (a + b > PP)
        return (a + b) % PP;
    return a + b;
}

int mul(int a, int b) // a * b with overflow checked
{
    a = a % PP;
    b = b % PP;
    if (a * b > PP)
        return (a * b) % PP;
    return a * b;
}

void calculate(int preva, int prevb, int i)
{
    printf("res = %d\n", res);
    if (i == n)
        return;
    if (b[i] == 0)
    {
        res = add(res, -a[i]);
        calculate(a[i], 0, i + 1);
    }
    else if (b[i] == 1)
    {
        res = add(res, a[i]);
        calculate(a[i], 1, i + 1);
    }
    else if (b[i] == 2)
    {
        if (prevb == 0)
        {
            int c = mul(preva, a[i]);
            res = add(add(res, preva), -c);
            calculate(c, 0, i + 1);
        }
        if (prevb == 1)
        {
            int c = mul(preva, a[i]);
            res = add(add(res, -preva), c);
            calculate(c, 1, i + 1);
        }
        if (prevb == -1)
        {
            res = mul(res, a[i]);
            calculate(res, -1, i + 1);
        }
    }
}

void solve()
{
    res = a[0];
    calculate(a[0], -1, 1);
    //res = res%P;
    printf("%d\n", res);
}

int main()
{
    input_from_file("EXPREVAL.INP");
    //input_from_STDIN();
    /*
    printf("%d\n", n);
    for (int i=0; i<n; i++) printf("%d ", a[i]);
    printf("\n");
    for (int i=1; i<n; i++) printf("%d ", b[i]);
    printf("\n");
    */
    solve();

    return 0;
}
