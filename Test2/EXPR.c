#include <stdio.h>
#define MAX 31
#define P 1000000007

int n;
int b;
int a[MAX];
int res;
int count;

void input_from_file(char* filename){
    FILE *f;
    f = fopen(filename, "r");
    if (f == NULL){
        printf("\nCannot open file %s\n", filename);
        return;
    }
    fscanf(f, "%d %d", &n, &b);
    for (int i=1; i<=n; i++) fscanf(f, "%d", &a[i]);
}

void input_from_STDIN(){
    scanf("%d %d", &n, &b);
    for (int i=1; i<=n; i++) scanf("%d", &a[i]);
}
/*
void calculate(int prev, int i){
    if (i == n+1){
        if (res == b) count++;
        return;
    }
    res = res + a[i];
    calculate(res, i+1);
    res = prev;

    res = res - a[i];
    calculate(res, i+1);
    res = prev;
}
*/
void calculate(int i){
    if (i == n+1){
        if (res == b) count++;
        return;
    }
    res = res + a[i];
    calculate(i+1);
    res = res - a[i];

    res = res - a[i];
    calculate(i+1);
    res = res + a[i];
}

void solve(){
    res = a[1];
    calculate(2);
    count = count%P;
    printf("%d\n", count);
}

int main(){
    input_from_file("EXPR.INP");
    //input_from_STDIN();

    solve();

    return 0;
}
