#include <stdio.h>
#define MAX 1001

int n;
int a[MAX];
int b[MAX];
//int countbetter[MAX];   //countbetter[i] is the number of students better than student i
int excellent[MAX]; //0 is excellent, 1 is not excellent

void input_from_file(char* filename){
    FILE *f;
    f = fopen(filename, "r");
    if (f == NULL){
        printf("\nCannot open file %s\n", filename);
        return;
    }
    fscanf(f, "%d", &n);
    for (int i=1; i<=n; i++) fscanf(f, "%d %d", &a[i], &b[i]);
}

void input_from_STDIN(){
    scanf("%d", &n);
    for (int i=1; i<=n; i++) scanf("%d %d", &a[i], &b[i]);
}

int better(int i, int j){
    if (a[i] > a[j] && b[i] > b[j]) return 1;   //student i is better than student j
    else if (a[i] < a[j] && b[i] < b[j]) return 2;  //student j is better than student i
    else return 0;
}

void solve(){
    int count = n;
    int i, j, check;
    for (i=1; i<=n; i++){
        for (j=i; j<=n; j++){
            check = better(i,j);
            if (check == 0) continue;
            if (check == 1){
                //countbetter[j]++;
                if (excellent[j] == 0) count--;
                excellent[j] = 1;
            }
            else if (check == 2){
                //countbetter[i]++;
                if (excellent[i] == 0) count--;
                excellent[i] = 1;
            }
        }
    }
    printf("%d\n", count);
}

int main(){
    input_from_file("SVG.INP");
    //input_from_STDIN();

    solve();

    return 0;
}
